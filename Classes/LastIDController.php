<?php
/**
 * Created by PhpStorm.
 * User: James-Work
 * Date: 11/5/2017
 * Time: 8:42 PM
 */

class LastIDController
{
    protected $deploymentLogID='';
    private $lastIDFile='last_updated_deployment_log_id.txt';
    protected $errorMsg='';


    function __construct()
    {
        $handler = fopen($this->lastIDFile,'r');
        if(!$handler)
        {
            $this->errorMsg = "Unable to read $this->lastIDFile, this could be because the file does not exist or could be something wrong with the file itself\n";
        }
        else
        {
            $this->deploymentLogID = fread($handler,filesize($this->lastIDFile));
            fclose($handler);
        }
        echo "end construction\n";
    }

    function keepLastID()
    {
        $handlew = fopen($this->lastIDFile,'w');
        if(!$handlew)
        {
            $this->errorMsg="Unable to write to $this->lastIDFile, please review file and make sure you have the appropriate permission to modify the file\n ";
        }
        else
        {
            fwrite($handlew,"$this->deploymentLogID");
            fclose($handlew);
        }


    }

    function setCurrentID($currentID)
    {
        if(gettype($currentID)=='string')
        {
            $this->deploymentLogID = $currentID;
        }
        else
        {
            settype($currentID,'string');
            $this->deploymentLogID = $currentID;
        }

    }

    function getCurrentID()
    {
        if(isset($this->deploymentLogID))
        {
            return $this->deploymentLogID;
        }
        else
        {
            $this->errorMsg="The value is currently not set";
            return false;
        }
    }

    function dasErr()
    {
        echo $this->errorMsg;
        $this->errorMsg="";
    }
}

/*$testing = new LastIDController();
$testing->dasErr();
$testing->setCurrentID('13171');
$testing->dasErr();
$testing->keepLastID();

$nextTest = new LastIDController();
$nextTest->dasErr();
$dasDistID = $nextTest->getCurrentID();
$nextTest->dasErr();
echo $dasDistID;*/
?>