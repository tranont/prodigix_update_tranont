<?php
/**
 * Created by PhpStorm.
 * User: James-Work
 * Date: 11/8/2017
 * Time: 10:58 AM
 */

class UpdateInvisusTaxbotDigix
{
    private $soapHeaderBody = array('AuthHeader'=>array('AuthorizationKey'=>'f06384138028e187046005070c49d78e'));
    private $domainHeader = 'http://www.prodigix.com';
    private $wsdl = 'http://member.tranont.com/ws/distributorws.asmx?WSDL';
    #constant private $soapAction = 'UpdateInvisusTaxbot';
    #constant private $location = 'member.tranont.com';
    #constant private $version = 2;
    private $options = array(
                                'soap_version'=>SOAP_1_2,
                                'encoding'=>'utf-8',
                                'trace'=>true
                                );
    protected $params = array();
    protected $distID;
    protected $invID;
    protected $taxbID;
    public $soapSetHead;
    public $errorMsgs = array();
    public $soapReq;
    public $soapResp;

    function __construct($distID, $invID, $taxbID)
    {
        ini_set('soap.wsdl_cache_enabled', 0);
        ini_set('soap.wsdl_cache_ttl', 900);
        ini_set('default_socket_timeout', 15);
        if(isset($distID))
        {
            $this->distID = $distID;
        }
        else
        {
            $this->errorMsgs [] = "The distributor Id was not set or was not passed appropriately";
        }

        if(isset($invID))
        {
            $this->invID = $invID;
        }
        else
        {
            $this->errorMsgs [] = "The Invisus Id was not set or was not passed appropriately";
        }

        if(isset($taxbID))
        {
            $this->taxbID = $taxbID;
        }
        else
        {
            $this->errorMsgs [] = "The Taxbot Id was not set or was not passed appropriately";
        }

        $this->params=array("DistID"=>"$distID", "InvisusID"=>"$invID", "TaxbotID"=>"$taxbID");

    }

    function callDigix()
    {
        try {
            $soap = new SoapClient($this->wsdl, $this->options);
            $dasHeaders = new SOAPHeader($this->domainHeader,'AuthHeader',$this->soapHeaderBody);
            $this->soapSetHead = $soap->__setSoapHeaders($dasHeaders);
            #echo "$soapSetHead";
            #$soapFunc = $soap->__getFunctions();
            #var_dump($soapFunc);
            #$soapDataTypes = $soap->__getTypes();
            #var_dump($soapDataTypes);
            #$data = $soap->__doRequest($requestBody, $location, $soapAction, $version, 0 );
            $soap->__soapCall('UpdateInvisusTaxbot', array("UpdateInvisusTaxbot" => $this->params));
            #$dataResp = $soap->__soapCall('UpdateInvisusTaxbotResponse');
            $this->soapReq = $soap->__getLastRequest();
            $this->soapResp = $soap->__getLastResponse();
        }
        catch(Exception $e) {
            die($e->getMessage());
        }

    }

}

#$updateRecord = new UpdateInvisusTaxbotDigix('2693','146282','256690');
#$updateRecord->callDigix();

#echo 'end';
#var_dump($updateRecord);

?>