<?php
/**
 * Created by PhpStorm.
 * User: James-Work
 * Date: 11/5/2017
 * Time: 8:28 PM
 */
include_once 'DasFuncs.php';

function DasConnection($user, $pass, $dbname, $location)
{
    /*8echo "my test message";
     = mysql_connect($location,$user,$pass);*/
    $dbStuffins = mysqli_connect($location, $user, $pass, $dbname);
    if (!$dbStuffins)
    {
        $failConn = "database connection failed" . PHP_EOL;
        $failConnErrNo = "Database Error No.: " . mysqli_connect_errno() . PHP_EOL;
        $failConnErrMsg = "Database Error Msg: " . mysqli_connect_error() . PHP_EOL;
        dasLog('Error',$failConn);
        dasLog('Error',$failConnErrNo);
        dasLog('Error',$failConnErrMsg);

        return false;
    }
    else
    {
        #echo "successful connection baby\n";
        return $dbStuffins;
    }

}


?>