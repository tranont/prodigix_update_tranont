<?php
/**
 * Created by PhpStorm.
 * User: James-Work
 * Date: 11/7/2017
 * Time: 11:36 PM
 */
include_once 'Conf/conf.php';
const logfile = 'DasLogFiles/update-invisus-taxbot.log';

function dasLog($loglvl, $logdata)
{

    $handlew = fopen(logfile,'a');
    if(!$handlew)
    {
        echo "Unable to write to log file, please review file and make sure you have the appropriate permission to modify the file\n ";
    }
    else
    {
        date_default_timezone_set('America/Denver');
        $dateTime = date("Y-m-d H:i:s");
        $logWrite = "$dateTime - $loglvl - $logdata\n";
        fwrite($handlew,"$logWrite");
        fclose($handlew);
    }
}

#$logTestData = "Testing Logging data 4";
#$testLogLevel = "INFO";

#dasLog($testLogLevel,$logTestData);
#echo "hold";