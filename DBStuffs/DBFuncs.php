<?php
/**
 * Created by PhpStorm.
 * User: James-Work
 * Date: 11/7/2017
 * Time: 11:33 PM
 */
include_once 'DasFuncs.php';

function getInvisID($dbConn, $currID)
{
    if (!$dbConn)
    {
        $dbErrHead = "Something is wrong with the database connection" . PHP_EOL;
        $dbErrNo = "Database Error No.: " . mysqli_errno($dbConn) . PHP_EOL;
        $dbErrMsg =  "Database Error Msg: " . mysqli_error($dbConn) . PHP_EOL;
        dasLog('Error',$dbErrHead);
        dasLog('Error',$dbErrNo);
        dasLog('Error',$dbErrMsg);
        return false;
    }
    else
    {
        $paramSQL = "SELECT ef.value as invisus_id FROM extra_fields as ef join deployment_logs as dl on ef.deployment_log_id=dl.id WHERE dl.user_id=$currID and ef.key='member_id' and dl.product_id IN (SELECT id FROM products where deployment_module='invisus')";
        $paramResult = mysqli_query($dbConn,$paramSQL);
        $paramAssoc = mysqli_fetch_assoc($paramResult);
        $returnID = $paramAssoc['invisus_id'];
        return $returnID;
    }

}

function getTaxbotID($dbConn, $currID)
{
    if (!$dbConn)
    {
        $dbErrHead = "Something is wrong with the database connection" . PHP_EOL;
        $dbErrNo = "Database Error No.: " . mysqli_errno($dbConn) . PHP_EOL;
        $dbErrMsg =  "Database Error Msg: " . mysqli_error($dbConn) . PHP_EOL;
        dasLog('Error',$dbErrHead);
        dasLog('Error',$dbErrNo);
        dasLog('Error',$dbErrMsg);
        return false;
    }
    else
    {
        $paramSQL = "SELECT ef.value as taxbot_id FROM extra_fields as ef join deployment_logs as dl on ef.deployment_log_id=dl.id WHERE dl.user_id=$currID and ef.key='userid' and dl.product_id=284";
        $paramResult = mysqli_query($dbConn,$paramSQL);
        $paramAssoc = mysqli_fetch_assoc($paramResult);
        $returnID = $paramAssoc['taxbot_id'];
        return $returnID;
    }

}

function getDeploymentIds($dbConn, $lastDepID)
{
    if (!$dbConn)
    {
        $dbErrHead = "Something is wrong with the database connection" . PHP_EOL;
        $dbErrNo = "Database Error No.: " . mysqli_errno($dbConn) . PHP_EOL;
        $dbErrMsg =  "Database Error Msg: " . mysqli_error($dbConn) . PHP_EOL;
        dasLog('Error',$dbErrHead);
        dasLog('Error',$dbErrNo);
        dasLog('Error',$dbErrMsg);
        return false;
    }
    else
    {
        $paramSQL = "SELECT dl.id from deployment_logs as dl WHERE dl.id>$lastDepID and (product_id IN (SELECT id FROM products where deployment_module='Invisus') OR product_id=284)";
        $paramResult = mysqli_query($dbConn,$paramSQL);
        #$paramAssoc = mysqli_fetch_assoc($paramResult);
        $returnDepList = array();
        while($row = mysqli_fetch_assoc($paramResult))
        {
            $returnDepList[] = $row['id'];
        }
        return $returnDepList;
    }
}

function getUserId($dbConn, $depID)
{

    if (!$dbConn)
    {
        $dbErrHead = "Something is wrong with the database connection" . PHP_EOL;
        $dbErrNo = "Database Error No.: " . mysqli_errno($dbConn) . PHP_EOL;
        $dbErrMsg =  "Database Error Msg: " . mysqli_error($dbConn) . PHP_EOL;
        dasLog('Error',$dbErrHead);
        dasLog('Error',$dbErrNo);
        dasLog('Error',$dbErrMsg);
        return false;
    }
    else
    {
        $paramSQL = "SELECT user_id from deployment_logs where id=$depID";
        $paramResult = mysqli_query($dbConn, $paramSQL);
        $paramAssoc = mysqli_fetch_assoc($paramResult);
        $returnID = $paramAssoc['user_id'];
        return $returnID;
    }
}

function getDistId($dbConn, $userID)
{

    if (!$dbConn) {
        $dbErrHead = "Something is wrong with the database connection" . PHP_EOL;
        $dbErrNo = "Database Error No.: " . mysqli_errno($dbConn) . PHP_EOL;
        $dbErrMsg =  "Database Error Msg: " . mysqli_error($dbConn) . PHP_EOL;
        dasLog('Error',$dbErrHead);
        dasLog('Error',$dbErrNo);
        dasLog('Error',$dbErrMsg);
        return false;
    }
    else
    {
        $paramSQL = "SELECT person_id from users where id=$userID";
        $paramResult = mysqli_query($dbConn, $paramSQL);
        $paramAssoc = mysqli_fetch_assoc($paramResult);
        $returnID = $paramAssoc['person_id'];
        return $returnID;
    }
}
?>