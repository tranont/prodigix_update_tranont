<?php
/**
 * Created by PhpStorm.
 * User: James-Work
 * Date: 10/31/2017
 * Time: 10:51 PM
 */

include_once 'Conf/conf.php';
include_once 'DasConnection.php';
include_once 'Classes/LastIDController.php';
include_once 'DBStuffs/DBFuncs.php';
include_once 'Classes/UpdateInvisusTaxbotDigix.php';

$processedUserIds = array();
$noDistID = false;

$myDBConn = dasConnection($GLOBALS['db_user'], $GLOBALS['db_pass'], $GLOBALS['db_name'], $GLOBALS['db_host']);

$idCtrl = new LastIDController;
$startingID = $idCtrl->getCurrentID();
dasLog('INFO',"Starting where we left off on $startingID");

if($startingID)
{
    $workingDepList = array();
    $workingDepList = getDeploymentIds($myDBConn, $startingID);
    if(isset($workingDepList) && count($workingDepList)>0)
    {
        foreach($workingDepList as $currDepID)
        {
            $idCtrl->setCurrentID($currDepID);
            $currUserID = getUserId($myDBConn, $currDepID);
            if(in_array($currUserID,$processedUserIds))
            {
                #echo "Already Processed User ID $currUserID\n";
                dasLog('INFO', "Already processed User ID $currUserID");
            }
            else
            {

                $workingDistID = getDistID($myDBConn, $currUserID);
                if(isset($workingDistID))
                {
                    echo "Got a Dist ID $workingDistID\n";
                }
                else
                {
                    #echo "No Dist id - without a dist id we cannot proceed ABORT!\n";
                    dasLog('ERROR', 'No Distributor ID. Cannoto Proceed without valid Distributor ID.');
                    $noDistID = true;
                    #find a way to break out
                }

                $workingInvID = getInvisID($myDBConn, $currUserID);
                if(isset($workingInvID))
                {
                    echo "Got a working Invisus ID $workingInvID\n";
                }
                else
                {
                    #echo "No invisus id";
                    $workingInvID='';
                }

                $workingTaxID  = getTaxbotID($myDBConn, $currUserID);
                if(isset($workingTaxID))
                {
                    echo "Got working TaxBot ID $workingTaxID\n";
                }
                else
                {
                    #echo "No Taxbot id";
                    $workingTaxID='';
                }

                if(!$noDistID)
                {
                    #$currParams = array("DistID"=>"$workingDistID", "InvisusID"=>"$workingInvID", "TaxbotID"=>"$workingTaxID");
                    $callDigix = new UpdateInvisusTaxbotDigix($workingDistID,$workingInvID,$workingTaxID);
                    dasLog('INFO', "Updating prodigix for Dist ID: $workingDistID, Invisuse id: $workingInvID, Taxbot id: $workingTaxID");
                    if(count($callDigix->errorMsgs))
                    {
                        foreach($callDigix->errorMsgs as $constructErrors)
                        {
                            dasLog('WARN',$constructErrors);
                        }
                    }
                    $callDigix->callDigix();
                    dasLog('INFO', "#####################Start Soap Request for $workingDistID#####################");
                    dasLog('INFO', $callDigix->soapReq);
                    dasLog('INFO', "#####################End Soap Request for $workingDistID#####################");
                    dasLog('INFO', "#####################Start Soap Response for $workingDistID#####################");
                    dasLog('INFO', $callDigix->soapResp);
                    dasLog('INFO', "#####################End Soap Response for $workingDistID#####################");
                }
                else
                {
                    dasLog('Error',"Cannot Call Prodigix without a Dist ID. Error for CDS User ID $currUserID");
                }
                $noDistID=false;
                $processedUserIds [] = $currUserID;
                $idCtrl->keepLastID();
            }

        }

    }
    else
    {
        $noNewMsg = "No new deployments for Invisus or Taxbot";
        dasLog('INFO',$noNewMsg);
    }

}
else
{
    dasLog('INFO', "Unable to get starting ID - Check logs and application to resolve");
}

mysqli_close($myDBConn);
exit();



#echo "$params";
#echo $params[];


/*$requestBody = '<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Header>
    <AuthHeader Domain="string" xmlns="http://www.prodogix.com/">
      <AuthorizationKey>'.$params['AuthorizationKey'].'</AuthorizationKey>
    </AuthHeader>
  </soap:Header>
  <soap:Body>
    <UpdateInvisusTaxbot xmlns="http://www.prodogix.com/">
      <DistID>'.$params['DistID'].'</DistID>
      <InvisusID>'.$params['InvisusID'].'</InvisusID>
      <TaxbotID>'.$params['TaxbotID'].'</TaxbotID>
    </UpdateInvisusTaxbot>
  </soap:Body>
</soap:Envelope>';
*/



?>